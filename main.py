import json
import os
import struct
import time
import uuid

import asks
import trio

CLIENT_ID = 'CLIENT ID'
CLIENT_SECRET = 'CLIENT SECRET'
REDIRECT = 'AN OAUTH2 REDIRECT'
CHANNEL_ID = '110373943822540800'


async def send_to_conn(conn: trio.abc.Stream, opcode, payload):
    payload['nonce'] = str(uuid.uuid4())
    json_part = json.dumps(payload).encode()
    packet = struct.pack('<II', opcode, len(json_part)) + json_part
    await conn.send_all(packet)


async def read_from_conn(conn: trio.abc.Stream):
    opcode, length = struct.unpack('<II', await conn.receive_some(8))
    payload = await conn.receive_some(length)
    return opcode, json.loads(payload.decode('utf-8'))


async def reader(conn: trio.abc.Stream):
    while True:
        thing = (await read_from_conn(conn))[1]
        if thing.get('evt') == 'MESSAGE_CREATE':
            message = thing['data']['message']
            author = message['author']['username'] + '#' + message['author']['discriminator']
            print('{:20}  : {}'.format(author, message['content']))
        else:
            print(thing)


async def main():
    envs = ['XDG_RUNTIME_DIR', 'TMPDIR', 'TMP', 'TEMP']
    connection = None
    prefix = None

    for env_var in envs:
        if os.getenv(env_var):
            prefix = os.getenv(env_var)

    if not prefix:
        prefix = '/tmp'

    for i in range(10):
        loc = f'{prefix}/discord-ipc-{i}'

        try:
            print(f'trying {loc}')
            conn = await trio.open_unix_socket(loc)
        except OSError:
            pass
        else:
            print(f'decided on {loc}')
            connection = conn
            break

    if not connection:
        return

    async with trio.open_nursery() as nursery:
        await send_to_conn(connection, 0, {'v': 1, 'client_id': CLIENT_ID})
        print(await read_from_conn(connection))

        # authenticate
        await send_to_conn(connection, 1, {
            'cmd': 'AUTHORIZE',
            'args': {
                'client_id': CLIENT_ID,
                'scopes': ['rpc']
            }
        })
        oauth_res = await read_from_conn(connection)
        print(oauth_res)
        code = oauth_res[1]['data']['code']

        token = (await asks.post('https://discord.com/api/oauth2/token', data={
            'grant_type': 'authorization_code',
            'code': code,
            'redirect_uri': REDIRECT,
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET
        })).json()['access_token']

        await send_to_conn(connection, 1, {
            'args': {
                'access_token': token
            },
            'cmd': 'AUTHENTICATE'
        })
        print(await read_from_conn(connection))

        await send_to_conn(connection, 1, {
            'cmd': 'SET_ACTIVITY',
            'args': {
                'pid': 1,  # TODO: psutil
                'activity': {
                    'state': 'Playing with the IPC',
                    # 'timestamps': {
                    #     'start': int(time.time() * 1_000),
                    #     'end': int(time.time() * 1_000) + 10_000
                    # },
                    'assets': {
                        'large_image': 'stuwupid',
                        'large_text': 'Nya (? is that what they say?)'
                    },
                    'buttons': [
                        {
                            'label': 'site',
                            'url': 'https://helvetica.moe/'
                        },
                        {
                            'label': 'cool clicky thing',
                            'url': 'https://www.swarmsim.com/'
                        }
                    ]
                }
            }
        })
        print(await read_from_conn(connection))

        # await send_to_conn(connection, 1, {
        #     'evt': 'MESSAGE_CREATE',
        #     'cmd': 'SUBSCRIBE',
        #     'args': {
        #         'channel_id': CHANNEL_ID
        #     }
        # })
        # print(await read_from_conn(connection))

        nursery.start_soon(reader, connection)


if __name__ == '__main__':
    trio.run(main)
